## [Mert Nuhoglu - Programmer, Data Scientist, Thinker](http://mertnuhoglu.com).

This is the Hugo source code for [Mert Nuhoglu](http://mertnuhoglu.com) website. This repository is a fork of [Peter Y. Chuang - Novelist](https://github.com/peterychuang/peterychuang.github.io). Please check his personal [website](https://novelist.xyz). Most of the design and images are his products. In addition to this usable web site design, he writes very useful stuff in technology as well. 

### Freedom to Participate and License

Any work (images, writings, presentations, ideas or whatever) which I own is always provided under <a href="http://creativecommons.org/licenses/by-sa/3.0/" rel="license">Creative Commons Attribution-Share Alike 3.0 License</a>

Unless otherwise specified (in the pages that references the images), most images in the following directory are taken from [Unsplash](https://unsplash.com/), other free CC0 sources, and taken by Peter Chuang, which he releases under CC0 license.

```
static/assets/img
```

All other directories and files are MIT Licensed. Feel free to use and modify the source codes for your own use. If you do use them, a link back to https://novelist.xyz would be appreciated, but is not required.

[MIT License](https://github.com/peterychuang/peterychuang.github.io/blob/source/LICENSE)

