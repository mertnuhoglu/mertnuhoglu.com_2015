
-- data generated by /usr/local/bin/datafiller version 2.0.0 (r792 on 2014-03-23) for postgresql

-- fill table order_line (2)
\echo # filling table order_line (2)
COPY order_line (shipment_date,irsaliye_kg,tesis,yas_toz) FROM STDIN (ENCODING 'utf-8');
2018-01-20 19:19:50	irsaliye_k	tesis_1_	FALSE
2018-01-20 19:18:50	irsaliye_k	tesis_2_2_2_	FALSE
\.

-- fill table plan (2)
\echo # filling table plan (2)
COPY plan (title) FROM STDIN (ENCODING 'utf-8');
title_1_1_1_
title_1_1_1_
\.

-- fill table pln_orl (4)
\echo # filling table pln_orl (4)
COPY pln_orl () FROM STDIN (ENCODING 'utf-8');




\.

-- restart sequences

-- analyze modified tables
ANALYZE order_line;
ANALYZE plan;
ANALYZE pln_orl;
