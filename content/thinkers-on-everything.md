---
layout: novelists-on-everything
description: A collection of great quotes from great thinkers and writers
header:
  caption: null
  caption_url: null
  image: novelist.jpg
title: What great thinkers say about everything
url: /thinkers-on-everything/
---

This page presents the thoughtful ideas from thinkers and writers.
