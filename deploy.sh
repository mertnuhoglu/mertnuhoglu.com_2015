#!/bin/sh
set -e
USER=mertnuhoglu
HOST=mertnuhoglu.com
DIR=/srv/www/mertnuhoglu.com/html

R -e 'blogdown::build_site()'
hugo && rsync -avz --delete public/ ${USER}@${HOST}:${DIR}

exit 0

